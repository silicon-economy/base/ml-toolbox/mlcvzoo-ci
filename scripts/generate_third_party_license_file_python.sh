#!/bin/sh

GENERATED_LICENSES_FILE="${GENERATED_LICENSES_FILE:=third-party-licenses/third-party-licenses.csv}"
UV_REQUIREMENTS_FILE_WITHOUT_DEV="${UV_REQUIREMENTS_FILE_WITHOUT_DEV:=requirements_locked/requirements-lock-uv-py310-without-dev.txt}"
pip3 uninstall -y "$(basename $(pwd))"
uv pip sync "$UV_REQUIREMENTS_FILE_WITHOUT_DEV"
pip3 install pip-licenses
pip-licenses --ignore-packages pkg-resources PyGObject chardet dbus-python ssh-import-id distro uv tomli --with-urls --format=csv --output-file="$GENERATED_LICENSES_FILE"
