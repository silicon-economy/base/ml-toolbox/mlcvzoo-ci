#!/bin/sh
# Shell script to check if versions of packages in a complementenry license file match
# the versions of the respective entries in the corresponding generated license file

# Usage: complementary_license_check.sh [third-party-license-file] [complementary-license-file]

# It expects the complementary license file to be a CSV file with lines such as "Name","Version",[additional info].
# It expects the generated license file to have a package identifier and its version in the same line but does not
# care about the rest.
# It informs about but accepts entries in the complementary file which don't have a corresponding entry in the
# generated license file.
# It exits with exit code 0 in case all found versions matched and exits with exit code 1 in case at least one
# package version differed between complementary file and generated file.

if [ $# -lt 2 ]; then
  echo "Usage: $0 third-party-license-file complementary-license-file"
  return 1
fi

# Skip any comments, and skip the first (header) line, format output with commas again
grep -v "^#" "$2" 2>/dev/null | tail -n +2 | awk -F, 'BEGIN{OFS=" "}{$1=$1; print $1","$2}' | {
  FAILED=0
  # Set internal field separator to our CSV delimiter (or at least the one emitted by awk)
  IFS=","
  while read -r pkg version; do
    # Strip quotation marks from read in tokens
    pkg=$(echo "$pkg" | tr -d '"')
    version=$(echo "$version" | tr -d '"')
    pkg_line=$(grep "$pkg" "$1" 2>/dev/null)
    if [ -n "$pkg_line" ]; then
      if ! echo "$pkg_line" | grep "$version" >/dev/null 2>&1; then
        echo "Version mismatch: complementary-file lists dependency '$pkg' in version '$version' but the corresponding generated entry is '$pkg_line'"
        FAILED=1
      fi
    else
      echo "There is no entry for '$pkg' in $1, please review manually"
    fi
  done
  exit $FAILED
}
