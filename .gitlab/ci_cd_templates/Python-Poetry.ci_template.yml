variables:
  ##################################################################################################
  # Definition of variables for project-specific configuration of the pipeline jobs.
  # These variables can be overridden as needed by jobs extending the job templates.
  ##################################################################################################
  # [RULES] Path to the project directory (relative to ${CI_PROJECT_DIR}).
  # Can either be empty or has to end with a `/`.
  PROJECT_DIR: ""
  # Path to the source files directory (relative to ${CI_PROJECT_DIR})
  SOURCE_FILES_DIR: "src/"
  # Path to the source files directory (relative to ${CI_PROJECT_DIR})
  TEST_FILES_DIR: "${SOURCE_FILES_DIR}tests/"
  # The name to use for the project in SonarQube.
  SONAR_PROJECT_NAME: ""
  # The key to use for the project in SonarQube.
  SONAR_PROJECT_KEY: ""
  # The name of the image to build and push (including any prefixes, e.g. "my/org/image-name").
  IMAGE_NAME: ""
  # The complete name of the image (including the registry's host and port).
  REGISTRY_IMAGE: "${NEXUS_DOCKER_REGISTRY}:${NEXUS_DOCKER_REGISTRY_PORT}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
  LATEST_REGISTRY_IMAGE: "${NEXUS_DOCKER_REGISTRY}:${NEXUS_DOCKER_REGISTRY_PORT}/${IMAGE_NAME}:latest"
  # Change the tag to use a runner with a specific capability if needed
  TEST_RUNNER_TAG: "docker"
  # Determine whether filling out the replacement config template is necessary for unit tests
  TEST_NEEDS_REPLACEMENT_CONFIG: "false"
  # Arguments to configure flake8
  FLAKE8_ARGS: ""

stages:
  - check
  - test
  - qa
  - build
  - archive

.python-poetry-qa-rules:
  rules:
    - if: $CI_MERGE_REQUEST_IID
      # Jobs in MRs are generally not allowed to fail
      allow_failure: false
      when: always
    - if: "$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH"
      variables:
        REGISTRY_IMAGE: "$LATEST_REGISTRY_IMAGE"
      # Jobs in default branches are generally allowed to fail to not prevent release archival
      allow_failure: true
      when: always
    - when: always
      # "Default clause" meaning: in all other cases - mere branches are allowed to fail everything
      allow_failure: true

.black:
  rules:
    !reference [.python-poetry-qa-rules, rules]
  interruptible: true
  image: $REGISTRY_IMAGE
  stage: check
  script:
    - '[ ! -z "${PROJECT_DIR}" ] && cd "${PROJECT_DIR}"'
    - poetry run black --check --diff "$SOURCE_FILES_DIR"
  tags:
    - docker

.isort:
  rules:
    !reference [.python-poetry-qa-rules, rules]
  interruptible: true
  image: $REGISTRY_IMAGE
  stage: check
  script:
    - '[ ! -z "${PROJECT_DIR}" ] && cd "${PROJECT_DIR}"'
    - poetry run isort --profile "black" --check-only "$SOURCE_FILES_DIR"
  tags:
    - docker

.type-checks-mypy:
  rules:
    !reference [.python-poetry-qa-rules, rules]
  interruptible: true
  image: $REGISTRY_IMAGE
  stage: check
  script:
    - '[ ! -z "${PROJECT_DIR}" ] && cd "${PROJECT_DIR}"'
    - poetry run mypy --install-types --non-interactive "$SOURCE_FILES_DIR"
  artifacts:
    reports:
      junit:
        - ${PROJECT_DIR}xunit-reports/xunit-result-mypy.xml
    paths:
      - ${PROJECT_DIR}xunit-reports/xunit-result-mypy.xml
    expire_in: 1 hour
  tags:
    - docker

.flake8:
  rules:
    !reference [ .python-poetry-qa-rules, rules ]
  interruptible: true
  image: $REGISTRY_IMAGE
  stage: check
  script:
    - '[ ! -z "${PROJECT_DIR}" ] && cd "${PROJECT_DIR}"'
    - poetry run flake8 $FLAKE8_ARGS "$SOURCE_FILES_DIR"
  tags:
    - docker

.pylint:
  rules:
    !reference [.python-poetry-qa-rules, rules]
  interruptible: true
  image: $REGISTRY_IMAGE
  stage: check
  script:
    - '[ ! -z "${PROJECT_DIR}" ] && cd "${PROJECT_DIR}"'
    - |-
       poetry run pylint --exit-zero "$SOURCE_FILES_DIR" -r n --msg-template="{path}:{line}: [ {msg_id}({symbol}), {obj} ] {msg}" | tee pylint.txt
  artifacts:
    paths:
      - ${PROJECT_DIR}pylint.txt
    expire_in: 1 hour
  tags:
    - docker

.third-party-license-check:
  rules:
    - if: $CI_MERGE_REQUEST_IID
      allow_failure: false
      when: always
    - if: "$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH"
      variables:
        REGISTRY_IMAGE: "$LATEST_REGISTRY_IMAGE"
      # The default branch may not emit artifacts with unclear licensing status
      allow_failure: false
      when: always
    # "Default clause" meaning: in all other cases
    - when: always
      allow_failure: true
  interruptible: true
  image: $REGISTRY_IMAGE
  stage: check
  variables:
    REFERENCE_LICENSES_FILE: third-party-licenses/third-party-licenses.csv
    GENERATED_LICENSES_DIR: generated/
    GENERATED_LICENSES_FILE: generated/third-party-licenses.csv
  script:
    - '[ ! -z "${PROJECT_DIR}" ] && cd "${PROJECT_DIR}"'
    - mkdir -p $GENERATED_LICENSES_DIR
    # This is a script so that what the user runs is in sync with the CI run
    - sh /scripts/generate_third_party_license_file.sh
    - 'cmp --silent $REFERENCE_LICENSES_FILE $GENERATED_LICENSES_FILE || export LICENSES_CHANGED=true'
    - 'if [ ! -z ${LICENSES_CHANGED} ]; then
        echo Some licenses used by the third-party dependencies have changed.;
        echo Please refer to the README and generate/update them accordingly.;
        git diff --no-index --unified=0 $REFERENCE_LICENSES_FILE $GENERATED_LICENSES_FILE;
       else
        sh /scripts/complementary_license_check.sh "$GENERATED_LICENSES_FILE" "third-party-licenses/third-party-licenses-complementary.csv";
       fi'
  artifacts:
    when: always
    paths:
      - ${PROJECT_DIR}${GENERATED_LICENSES_FILE}
      - ${PROJECT_DIR}third-party-licenses-complementary.csv
  tags:
    - docker

.unit-tests:
  rules:
    !reference [.python-poetry-qa-rules, rules]
  interruptible: true
  image: $REGISTRY_IMAGE
  stage: test
  script:
    - '[ ! -z "${PROJECT_DIR}" ] && cd "${PROJECT_DIR}"'
    - tree ${PROJECT_ROOT_DIR} || true
    - tree /build-env/data/modeling_data/ || true
    - - |-
        if [[ "$TEST_NEEDS_REPLACEMENT_CONFIG" == "true" ]]; then
          /scripts/gen_replacement_config.sh config/templates/replacement_config_template.yaml config/replacement_config.yaml
          cat config/replacement_config.yaml
        fi
    - pip3 list
    - poetry run pytest --cov="$SOURCE_FILES_DIR" --cov-branch --cov-report xml:coverage.xml --junitxml xunit-reports/xunit-result-pytest.xml "$TEST_FILES_DIR"
    - poetry run coverage xml
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    when: always
    reports:
      junit: ${PROJECT_DIR}xunit-reports/xunit-result-pytest.xml
      coverage_report:
        coverage_format: cobertura
        path: ${PROJECT_DIR}coverage.xml
    paths:
      - ${PROJECT_DIR}xunit-reports/xunit-result-pytest.xml
      - ${PROJECT_DIR}coverage.xml
  tags:
    - $TEST_RUNNER_TAG

.sonarqube-check:
  rules:
    !reference [.python-poetry-qa-rules, rules]
  interruptible: true
  stage: qa
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [ "" ]
  variables:
    # Define the location of the analysis task cache
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    # Tell git to fetch all the branches of the project, required by the analysis task
    GIT_DEPTH: "0"
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    # Unshallow the git repo clone, workaround for GIT_DEPTH: 0 sometimes being not enough
    - git fetch --unshallow || true
    - '[ ! -z "${PROJECT_DIR}" ] && cd "${PROJECT_DIR}"'
    - export PACKAGE_VERSION=$(python3 -c "exec(open(\"$SOURCE_FILES_DIR/__init__.py\").read()) ; print(__version__)" 2>/dev/null)
    # Exclude the tools packages "$SOURCE_FILES_DIR/tools/**" since those only contain
    # developer commandline tools that don't have to fulfill any test-coverage.
    - >
      sonar-scanner
      -Dsonar.python.xunit.reportPath=xunit-reports/xunit-result-pytest.xml
      -Dsonar.python.coverage.reportPaths=coverage.xml
      -Dsonar.sources=${SOURCE_FILES_DIR}
      -Dsonar.tests=${TEST_FILES_DIR}
      -Dsonar.coverage.exclusions=**__init__**,${TEST_FILES_DIR}**,config.py,manage.py,setup.py,${SOURCE_FILES_DIR}tools/**
      -Dsonar.exclusions=*.xml,${TEST_FILES_DIR}**
      -Dsonar.python.pylint.reportPath=pylint.txt
      -Dsonar.language=py
      -Dsonar.python.version=3
      -Dsonar.host.url=$SONAR_HOST_URL
      -Dsonar.login=$SONAR_TOKEN
      -Dsonar.projectName=${SONAR_PROJECT_NAME}
      -Dsonar.projectKey=${SONAR_PROJECT_KEY}
      -Dsonar.projectVersion=$PACKAGE_VERSION
      -Dsonar.gitlab.commit_sha=$CI_COMMIT_SHA
      -Dsonar.gitlab.ref_name=$CI_COMMIT_REF_NAME
      -Dsonar.gitlab.url=$CI_PROJECT_URL
      -Dsonar.gitlab.project_id=$CI_PROJECT_ID
      -Dsonar.qualitygate.wait=true
  tags:
    - docker

.build-poetry:
  rules:
    - if: $CI_MERGE_REQUEST_IID
      allow_failure: false
      when: always
    - if: "$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH"
      variables:
        REGISTRY_IMAGE: "$LATEST_REGISTRY_IMAGE"
      # No build -> nothing to deploy or release
      allow_failure: false
      when: always
    - when: always
      # "Default clause" meaning: in all other cases
      allow_failure: true
  interruptible: true
  stage: build
  image: $REGISTRY_IMAGE
  script:
    - '[ ! -z "${PROJECT_DIR}" ] && cd "${PROJECT_DIR}"'
    # Setup poetry version for each commit (not in master):
    # VERSION.dev+BRANCHNAME-COMMIT
    - |-
      if [[ ! $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH ]]; then
        poetry version "$(poetry version | awk '{print $2}').dev+$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
      fi
    - poetry build
  artifacts:
    paths:
      - ${PROJECT_DIR}dist/
  tags:
    - docker

.archive-internal:
  rules:
    - if: $CI_MERGE_REQUEST_IID
      allow_failure: false
      when: always
    - if: "$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH"
      variables:
        REGISTRY_IMAGE: "$LATEST_REGISTRY_IMAGE"
      # This seems counter-intuitve, but it is important that this can fail for pipeline repeatability, "re-releases" are rejected
      allow_failure: true
      when: always
    # "Default clause" meaning: in all other cases
    - when: always
      allow_failure: true
  stage: archive
  image: $REGISTRY_IMAGE
  script:
    - '[ ! -z "${PROJECT_DIR}" ] && cd "${PROJECT_DIR}"'
    # Skip uploading artifacts if they are already there to not override them in case this is possible
    - |-
      if ! pip download -i ${NEXUS_PYPI_REPO}simple --no-cache-dir --no-deps ${PROJECT_NAME}==$(poetry version | awk '{print $2}') > /dev/null 2>&1; then
        pip3 install twine colorama
        twine upload --verbose --repository-url ${NEXUS_PYPI_REPO} --username "$NEXUS_USER" --password "$NEXUS_PASS" dist/*.whl
        TWINE_USERNAME=gitlab-ci-token TWINE_PASSWORD=${CI_JOB_TOKEN} twine upload --skip-existing --verbose --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*
      fi
  tags:
    - docker

.archive-pypi:
  rules:
    - if: "$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH"
      # This seems counter-intuitve, but it is important that this can fail for pipeline repeatability, "re-releases" are rejected
      allow_failure: true
      when: always
    # "Default clause" meaning: in all other cases
    - when: never
  stage: archive
  image: $LATEST_REGISTRY_IMAGE
  script:
    - '[ ! -z "${PROJECT_DIR}" ] && cd "${PROJECT_DIR}"'
    # Skip uploading artifacts if they are already there to not override them in case this is possible
    - |-
      if ! pip download --no-cache-dir --no-deps ${PROJECT_NAME}==$(poetry version | awk '{print $2}') > /dev/null 2>&1; then
        pip3 install twine colorama
        TWINE_USERNAME=${PYPI_TOKEN_USERNAME} TWINE_PASSWORD=${PYPI_TOKEN_PASSWORD}  twine upload dist/*
      fi
  tags:
    - docker
