ARG SOURCE_IMAGE="nvidia/cuda:12.1.0-cudnn8-devel-ubuntu22.04"
FROM $SOURCE_IMAGE

# For NVIDIA Jetson devices, select one or more appropriate CUDA arch values below
# Look up your CUDA compute capability at: https://developer.nvidia.com/cuda-gpus
# Some common CUDA compute Capabilites for x86:
#  - Quadro P5000/Geforce RTX10*0: 6.1
#  - Tesla V100: 7.0
#  - Quadro T1000/Geforce RTX20*0: 7.5
#  - NVIDIA A100: 8.0
#  - RTX A*000/Geforce RTX3090: 8.6
# Some common CUDA compute capabilities for Jetson devices (arm64):
#  - Jetson Nano/TX1/Tegra X1: 5.3
#  - Jetson TX2: 6.2
#  - Jetson Xavier AGX: 7.2
ENV TORCH_CUDA_ARCH_LIST="6.1;7.0;7.5;8.0;8.6"
ENV CUDA_HOME="/usr/local/cuda/"

# Fetch updated NVIDIA keys
RUN apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/3bf863cc.pub && \
    apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/7fa2af80.pub

# Non-trivial packages:
# - gettext-base is needed for envsubst which is used in CI
# - libgl1-mesa-glx is needed by opencv-python, otherwise it
#   throws 'ImportError: libGL.so.1'
# - libprotobuf-dev and protobuf-compiler are needed by onnx
# - libpython3-dev is needed to link against the python interpreter (e.g. pycocotools)
# - libpq-dev is needed by postgresql
# - blas, lapack, fftw3 and hdf5 are needed by pytorch et. al.
RUN echo ">------ Update system, install packages" && \
    apt update && \
    DEBIAN_FRONTEND="noninteractive" apt install --no-install-recommends -y \
    build-essential gettext-base cmake ninja-build libprotobuf-dev protobuf-compiler \
    python3-pip python3-setuptools python3-venv libpython3-dev  \
    git tree curl unzip \
	pkg-config libgl1-mesa-glx libcap-dev libpq-dev libopencv-dev \
    libblas3 liblapack3 liblapack-dev libblas-dev gfortran libatlas-base-dev \
    libfftw3-bin libfftw3-dev libhdf5-dev libhdf5-mpi-dev libhdf5-openmpi-dev \
    && apt clean && rm -rf /var/lib/apt/lists/*

ENV PIP_NO_CACHE_DIR=1

# Setup and install poetry
ENV POETRY_VERSION="1.8.5"
RUN curl -sSL https://install.python-poetry.org | python3 - --version "$POETRY_VERSION"
ENV PATH="/root/.local/bin:$PATH"
# Add a plugin for using a pull-through caching mirror and disable parallel installation for robustness
RUN poetry self add poetry-plugin-pypi-mirror@^0.4 && \
    poetry config installer.parallel false && \
    poetry config --list
ENV POETRY_PYPI_MIRROR_URL="https://nexus.apps.sele.iml.fraunhofer.de/repository/pypi-group/simple"

# Install uv
RUN curl -LsSf https://astral.sh/uv/install.sh | sh
ENV PATH="/root/.cargo/bin:$PATH"

RUN pip config set global.index-url "https://nexus.apps.sele.iml.fraunhofer.de/repository/pypi-group/simple"

COPY scripts/ "/scripts"
RUN chmod -R ugo+rx /scripts/

# ====================================================================
# Label the image
LABEL org.opencontainers.image.authors="Maximilian Otten <maximilian.otten@iml.fraunhofer.de>, Christian Hoppe <christian.hoppe@iml.fraunhofer.de" \
      org.opencontainers.image.vendor="Fraunhofer IML" \
      org.opencontainers.image.title="MLCVZoo CI Image - Main GPU-enabled gitlab-runner container" \
      org.opencontainers.image.description="Container base image for GPU enabled integration testing and continuous delivery for the MLCVZoo"
